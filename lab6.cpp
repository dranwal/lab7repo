/* Dillon Ranwala
   Cpsc 1021 Lab Section 3, F20
	dranwal@clemson.edu
	TAs: Elliot McMillan and Victoria Xu
	Lab #6
   Program using C++ language and functions to input/output a struct array
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee arr[10];
	int i;
	
	// getting employee info from user
	for (i=0; i<10; i++) {
		cout << "Enter Employee " << i+1 << "'s last Name." << endl;
		cin >> arr[i].lastName;
	
		cout << "Enter Employee " << i+1 << "'s first Name." << endl;
		cin >> arr[i].firstName;

		cout << "Enter Employee " << i+1 << "'s birth year." << endl;
		cin >> arr[i].birthYear;

		cout << "Enter Employee " << i+1 << "'s hourly wage." << endl;
		cin >> arr[i].hourlyWage;
	}

  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	random_shuffle(&arr[0],&arr[10],myrandom);

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
	employee newEmp[5]= {arr[0],arr[1],arr[2],arr[3],arr[4]};

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
	sort(&newEmp[0], &newEmp[5],name_order);

    /*Now print the array below */
	
	cout << endl;
	for(auto emp: newEmp) {
		cout << setw(15) << right << emp.lastName +  ", " +
		emp.firstName << endl;

		cout << setw(15) << right << emp.birthYear << endl;
		cout << setw(15) << right << fixed << showpoint <<
		setprecision(2) << emp.hourlyWage << endl;
	}



  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
  // IMPLEMENT
  return(lhs.lastName < rhs.lastName);
}

